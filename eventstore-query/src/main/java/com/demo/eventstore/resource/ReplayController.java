package com.demo.eventstore.resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.eventstore.query.Replayer;

import java.text.NumberFormat;

@RestController
public class ReplayController {
    private Replayer replayer;

    public ReplayController(Replayer replayer) {
        this.replayer = replayer;
    }

    @PostMapping("/replay")
    public String replay() {
        replayer.replay("com.demo.eventstore.query");

        return "Replay started...";
    }

    @GetMapping("/replay")
    public String progress() {
        return replayer.getProgress("com.demo.eventstore.query").map(progress ->
                String.format("Replay at %d/%d (%s complete)",
                        progress.getCurrent(), progress.getTail(), NumberFormat.getPercentInstance().format(progress.getProgress().doubleValue()))
        ).orElse("No replay active");
    }
}
