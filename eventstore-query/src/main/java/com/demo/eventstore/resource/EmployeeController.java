package com.demo.eventstore.resource;

import com.demo.eventstore.repository.Employee;
import com.demo.eventstore.repository.EmployeeRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/query")

public class EmployeeController {

    private final EmployeeRepo repo;

    public EmployeeController(EmployeeRepo repo) {
        this.repo = repo;
    }

    @GetMapping("/{id}")
    public Employee getEmployee(@PathVariable String id) {
        return repo.findById(id).orElseThrow();

    }

    @GetMapping
    public List<Employee> updateEmployee() {
        return repo.findAll();
    }
}
