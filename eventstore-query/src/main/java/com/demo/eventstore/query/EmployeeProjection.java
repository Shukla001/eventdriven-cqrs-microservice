package com.demo.eventstore.query;

import com.demo.eventstore.repository.Employee;
import com.demo.eventstore.repository.EmployeeRepo;
import com.event.EmployeeCreatedEvt;
import com.event.EmployeeUpdatedEvt;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.ReplayStatus;
import org.axonframework.eventhandling.Timestamp;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.logging.Logger;

@Component
public class EmployeeProjection {

    private final EmployeeRepo repository;
    private Logger LOGGER = Logger.getLogger(EmployeeProjection.class.getName());

    public EmployeeProjection(EmployeeRepo repository) {
        this.repository = repository;
        LOGGER.info("Employee repo created" + repository);
    }

    @EventHandler
    public void onEmployeeCreated(EmployeeCreatedEvt evt, @Timestamp Instant timestamp, ReplayStatus replayStatus){
        Employee e = new Employee(evt.getId(), evt.getFirstName(), evt.getLastName(), evt.getStatus(), timestamp, Instant.now()) ;
        repository.save(e);
        LOGGER.info("Employee " + e.getId() + " Saved");
        if (replayStatus.isReplay()) {
            try {
                // if we are replaying, build in a little delay so we can see the progress in action a bit better
                Thread.sleep(2000L);
            } catch (InterruptedException ex) { /* shouldn't happen */ }
        }
    }

    @EventHandler
    public void onEmployeeUpdated(EmployeeUpdatedEvt evt, @Timestamp Instant timestamp, ReplayStatus replayStatus){
        Employee emp = repository.getOne(evt.getId());
        emp.setFirstName(evt.getFirstName());
        emp.setLastName(evt.getLastName());
        emp.setStatus(evt.getStatus());
        emp.setTimestamp(timestamp);
        emp.setLastUpdated(Instant.now());
        repository.save(emp);
        LOGGER.info("Employee " + emp.getId() + " Updated");
    }
}
