package com.demo.eventstore.query;

import org.axonframework.config.EventProcessingConfiguration;
import org.axonframework.eventhandling.ReplayToken;
import org.axonframework.eventhandling.TrackingEventProcessor;
import org.axonframework.eventhandling.TrackingToken;
import org.axonframework.eventhandling.tokenstore.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalLong;
import java.util.stream.IntStream;

@Component
public class Replayer {
    private EventProcessingConfiguration configuration;
    private TokenStore tokenStore;

    public Replayer(EventProcessingConfiguration configuration, TokenStore tokenStore) {
        this.configuration = configuration;
        this.tokenStore = tokenStore;
    }

    public void replay(String name) {
        configuration.eventProcessor(name, TrackingEventProcessor.class).ifPresent(processor -> {
            processor.shutDown();
            processor.resetTokens();
            processor.start();
        });
    }

    @Transactional
    public Optional<Progress> getProgress(String name) {
        int[] segments = tokenStore.fetchSegments(name);

        if (segments.length == 0) {
            return Optional.empty();
        } else {
            Progress accumulatedProgress = IntStream.of(segments).mapToObj(segment -> {
                TrackingToken token = tokenStore.fetchToken(name, segment);

                OptionalLong maybeCurrent = token.position();
                OptionalLong maybePositionAtReset = OptionalLong.empty();

                if (token instanceof ReplayToken) {
                    maybePositionAtReset = ((ReplayToken) token).getTokenAtReset().position();
                }

                return new Progress(maybeCurrent.orElse(0L), maybePositionAtReset.orElse(0L));
            }).reduce(new Progress(0, 0), (acc, progress) ->
                    new Progress(acc.getCurrent() + progress.getCurrent(), acc.getTail() + progress.getTail()));

            return (accumulatedProgress.getTail() == 0L) ? Optional.empty() : Optional.of(accumulatedProgress);
        }
    }

    public static class Progress {
        private long current;
        private long tail;

        public Progress() {
        }

        public Progress(long current, long tail) {
            this.current = current;
            this.tail = tail;
        }

        public long getCurrent() {
            return current;
        }

        public void setCurrent(long current) {
            this.current = current;
        }

        public long getTail() {
            return tail;
        }

        public void setTail(long tail) {
            this.tail = tail;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Progress progress = (Progress) o;
            return getCurrent() == progress.getCurrent() &&
                    getTail() == progress.getTail();
        }

        @Override
        public int hashCode() {
            return Objects.hash(getCurrent(), getTail());
        }

        public BigDecimal getProgress() {
            return BigDecimal.valueOf(current, 2).divide(BigDecimal.valueOf(tail, 2), RoundingMode.HALF_UP);
        }
    }
}
