package com.demo.eventstore.repository;



import com.event.EmployeeStatus;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Instant;


@Entity
public class Employee {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private EmployeeStatus status;
    private Instant timestamp;
    private Instant lastUpdated;

    public Employee() {
    }

    public Employee(String id, String firstName, String lastName, EmployeeStatus status, Instant timestamp, Instant lastUpdated) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.timestamp = timestamp;
        this.lastUpdated = lastUpdated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public Instant getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Instant lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
