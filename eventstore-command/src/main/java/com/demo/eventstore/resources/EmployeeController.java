package com.demo.eventstore.resources;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.*;

import com.event.CreateEmployeeCmd;
import com.event.UpdateEmployeeCmd;

import io.swagger.annotations.Api;

import java.util.UUID;

@RestController
@RequestMapping("/command")
@Api(value = "Employee Commands", tags = "Employee Commands")
public class EmployeeController {

    private final CommandGateway commandGateway;

    public EmployeeController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping
    public String createEmployee(@RequestBody CreateEmployeeCmd employee) {
        employee.setId(UUID.randomUUID().toString());
        return commandGateway.sendAndWait(employee);

    }

    @PutMapping
    public void updateEmployee(@RequestBody UpdateEmployeeCmd employeeCmd) {
        commandGateway.send(employeeCmd);
    }
}
