package com.demo.eventstore.command;

import com.event.*;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.spring.stereotype.Aggregate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

@Aggregate
public class Employee {

    @AggregateIdentifier
    private String id;
    private String firstName;
    private String lastName;
    private EmployeeStatus status;

    public Employee() {
    }

    public Employee(String id, String firstName, String lastName, EmployeeStatus status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
    }

    @CommandHandler
    public Employee(CreateEmployeeCmd cmd){
        EmployeeCreatedEvt evt = new EmployeeCreatedEvt(cmd.getId(), cmd.getFirstName(), cmd.getLastName(), cmd.getStatus());
        apply(evt);
    }

    @CommandHandler
    public void handle(UpdateEmployeeCmd cmd){
        apply(new EmployeeUpdatedEvt(cmd.getId(), cmd.getFirstName(), cmd.getLastName(), cmd.getStatus()));
    }

    @CommandHandler
    public void handle(DeleteEmployeeCmd cmd){
        apply(new EmployeeDeletedEvt(cmd.getId()));
    }

    @EventSourcingHandler
    public void on(EmployeeCreatedEvt evt){
        id = evt.getId();
        firstName = evt.getFirstName();
        lastName = evt.getLastName();
        status = evt.getStatus();

    }
    @EventSourcingHandler
    public void on(EmployeeUpdatedEvt evt){
        id = evt.getId();
        firstName = evt.getFirstName();
        lastName = evt.getLastName();
        status = evt.getStatus();

    }
    @EventSourcingHandler
    public void on(EmployeeDeletedEvt evt){
        id = evt.getId();
        status = EmployeeStatus.DELETED;
    }
}
