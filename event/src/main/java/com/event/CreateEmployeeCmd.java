package com.event;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class CreateEmployeeCmd {

    @TargetAggregateIdentifier
    private String id;
    private String firstName;
    private String lastName;
    private EmployeeStatus status;

    public CreateEmployeeCmd() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }
}
