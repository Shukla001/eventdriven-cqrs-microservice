package com.event;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class DeleteEmployeeCmd {

    @TargetAggregateIdentifier
    private String id;

    public DeleteEmployeeCmd(String id) {
        this.id = id;
    }

    public DeleteEmployeeCmd() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
