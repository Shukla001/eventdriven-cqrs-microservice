package com.event;

public class EmployeeUpdatedEvt {

    private String id;
    private String firstName;
    private String lastName;
    private EmployeeStatus status;

    public EmployeeUpdatedEvt() {
    }

    public EmployeeUpdatedEvt(String id, String firstName, String lastName, EmployeeStatus status) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public EmployeeStatus getStatus() {
        return status;
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
    }
}

