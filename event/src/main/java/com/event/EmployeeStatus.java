package com.event;

public enum EmployeeStatus {
    ACTIVE, INACTIVE, DELETED
}
