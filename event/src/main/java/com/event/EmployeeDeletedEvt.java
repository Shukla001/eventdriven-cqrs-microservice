package com.event;

public class EmployeeDeletedEvt {

    private String id;

    public EmployeeDeletedEvt() {
    }

    public EmployeeDeletedEvt(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
